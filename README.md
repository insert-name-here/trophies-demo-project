# Trophies Demo Project For Hackathon!

This project is being used to demo the functionality and features of the [Trophies](https://gitlab.com/insert-name-here/trophies) project.

## Component Parts of the Trophies project

- The [trophies-manager](https://gitlab.com/insert-name-here/trophies-manager/-/tree/feature/token-redemption) project contains the code for setting up all the infrastructure, DB, Functions and API's for trophies.
- The [trophies-gitlab-integration](https://gitlab.com/insert-name-here/trophies-manager/-/tree/feature/gitlab_hook_issue_token) this contains a Function and API for a Gitlab push event.
- The [trophies-feeder](https://gitlab.com/insert-name-here/trophies-feeder/-/tree/feature/trophies-feeder) integrates the Investec Open Banking API with the trophies-manager to issue funds to a project.
- The [trophies-portal](https://gitlab.com/insert-name-here/trophies/-/tree/4-web-portal-mobile-app) this is a front-end to access various functions of the trophies platform such as viewing and claiming trophies.

## Steps to use trophies
**NOTE**

The steps below assume that you will be using the infrastructure and API's that have already been setup by `insert-name-here`. Should you wish to have your own you can follow the steps in each of the various components above to set them up for yourself.

**STEPS**
- Navigate to the [Trophies Portal](https://trophies.industrialcuriosity.com/).
- Register or Login to the portal
- Create a new project
- View the project to obtain the following values:
    - Project ID
    - API Key
    - Funding API Key
- Follow these steps for Gitlab push event integration (**NB**: Assumes Gitlab project and repository has been setup)
    - Navigate to your project settings webhook page in Gitlab
    - In the URL box input the following URL: `https://e64uw9g511.execute-api.af-south-1.amazonaws.com/prod/projects/{ProjectId}/trophies`
    - In the Secret Token box input your API Key
    - Ensure that 'Push Events' is ticked. Update the branch box to indicate what branch you want the event to be fired on.
    - Scroll to the bottom and uncheck 'Enable SSL verification' box
    - Save
----
At this point pushes to the branch in the repo setup will allocate trophies based on the `trophies.tracking.json` file submitted with the push. Next is to allocate funds to the project, to do this using Investec Open Banking API follow these steps:
- Pull the [trophies-feeder](https://gitlab.com/insert-name-here/trophies-feeder/-/tree/feature/trophies-feeder) project and follow the steps to set it up.
- Update environment variables to point to the relevant Investec Account, ProjectId and Funding API Key.
- Execute the feeder to check for any matching transactions that will allocate funds. This can be setup on a schedule to avoid manual intervention.
---
Once funds are allocated to the project, contributors will be able to claim funds using the [Trophies Portal](https://trophies.industrialcuriosity.com/).
